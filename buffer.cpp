
typedef struct {
	char *text;
	uint64_t length;
	uint64_t gap_start;
	uint64_t gap_end;
} buffer_t;

void buffer_create(buffer_t *buffer, uint64_t size) {
	buffer->text = (char *) malloc(size);
	buffer->length = 0;
	buffer->gap_start = 0;
	buffer->gap_end = size;
}

static void buffer_shift_gap(buffer_t *buffer, uint64_t position) {
	if (position < buffer->gap_start) {
		uint64_t gap_delta = buffer->gap_start - position;
		buffer->gap_start -= gap_delta;
		buffer->gap_end -= gap_delta;
		memmove(buffer->text + buffer->gap_end, buffer->text + buffer->gap_start, gap_delta);
	} else if (position > buffer->gap_start) {
		uint64_t gap_delta = position - buffer->gap_start;
		memmove(buffer->text + buffer->gap_start, buffer->text + buffer->gap_end, gap_delta);
		buffer->gap_start += gap_delta;
		buffer->gap_end += gap_delta;
	}
}

uint64_t buffer_capacity(buffer_t *buffer) {
	return buffer->gap_end - buffer->gap_start;
}

static uint64_t buffer_allocated_size(buffer_t *buffer) {
	return buffer_capacity(buffer) + buffer->length;
}

static void buffer_ensure_can_hold(buffer_t *buffer, uint64_t size) {
	if (buffer_capacity(buffer) < size) {
		uint64_t new_size = 2 * buffer_allocated_size(buffer) + 1;
		assert(new_size >= buffer_allocated_size(buffer) + size);
		buffer->text = (char *) realloc(buffer->text, new_size);
		assert(buffer->text);
		buffer_shift_gap(buffer, buffer->length);
		buffer->gap_end = new_size;
		assert(buffer_allocated_size(buffer) == new_size);
	}
}

void buffer_insert(buffer_t *buffer, uint64_t position, char *characters, uint64_t length) {
	assert(position <= buffer->length);
	buffer_ensure_can_hold(buffer, length);
	buffer_shift_gap(buffer, position);
	memcpy(buffer->text + buffer->gap_start, characters, length);
	buffer->gap_start += length;
	buffer->length += length;
}

void buffer_remove_left(buffer_t *buffer, uint64_t position, uint64_t length) {
	assert(buffer->length >= length);
	buffer_shift_gap(buffer, position);
	buffer->gap_start -= length;
	buffer->length -= length;
}

void buffer_remove_right(buffer_t *buffer, uint64_t position, uint64_t length) {
	assert(buffer->length >= length);
	buffer_shift_gap(buffer, position);
	buffer->gap_end += length;
	buffer->length -= length;
}

char buffer_char_at(buffer_t *buffer, uint64_t position) {
	assert(buffer->length >= position);
	if (position < buffer->gap_start) {
		return buffer->text[position];
	} else {
		return buffer->text[buffer->gap_end + (position - buffer->gap_start) + 1];
	}
}

void buffer_destroy(buffer_t *buffer) {
	free(buffer->text);
	buffer->text = NULL;
	buffer->length = 0;
	buffer->gap_start = 0;
	buffer->gap_end = 0;
}
