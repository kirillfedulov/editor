#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <inttypes.h>
#include <assert.h>

#include <SDL.h>
#include <SDL_ttf.h>

#include "buffer.cpp"

void die(const char *fmt, ...) {
	va_list args;
	va_start(args, fmt);
	vprintf(fmt, args);
	va_end(args);
	putchar('\n');
	exit(-1);
}

int main() {
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
		die("SDL_Init failed: %s", SDL_GetError());
	int window_width = 800, window_height = 600;
	SDL_Window *window = SDL_CreateWindow("Edit", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, window_width, window_height, SDL_WINDOW_RESIZABLE);
	if (!window)
		die("SDL_CreateWindow failed: %s", SDL_GetError());
	SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 0);
	if (!renderer)
		die("SDL_CreateRenderer failed: %s", SDL_GetError());
	if (TTF_Init() != 0)
		die("TTF_Init failed: %s", TTF_GetError());
	TTF_Font *font = TTF_OpenFont("Inconsolata-Regular.ttf", 18);
	if (!font)
		die("TTF_OpenFont failed: %s", TTF_GetError());
	SDL_Color background_color = {255, 255, 255, 255};
	SDL_Color foreground_color = {0, 0, 0, 0};
	const uint32_t MAX_GLYPHS = 128;
	struct {
		SDL_Texture *texture;
		uint64_t width, height;
	} glyphs[MAX_GLYPHS] = {};
	for (uint32_t i = 0; i < MAX_GLYPHS; i++) {
		SDL_Surface *surface = TTF_RenderGlyph32_Shaded(font, i, foreground_color, background_color);
		if (surface) {
			glyphs[i].texture = SDL_CreateTextureFromSurface(renderer, surface);
			if (!glyphs[i].texture)
				die("SDL_CreateTextureFromSurface failed: %s", SDL_GetError());
			int width, height;
			if (SDL_QueryTexture(glyphs[i].texture, NULL, NULL, &width, &height) != 0)
				die("SDL_QueryTexture failed: %s", SDL_GetError());
			glyphs[i].width = width;
			glyphs[i].height = height;
			SDL_FreeSurface(surface);
		}
	}
	buffer_t buffer;
	buffer_create(&buffer, 32);
	uint64_t cursor = 0;
	int cursor_column = 0;
	int cursor_row = 0;
	int cursor_width = 0;
	int cursor_height;
	bool running = true;
	SDL_StartTextInput();
	while (running) {
		SDL_Event event;
		if (SDL_WaitEvent(&event) != 1)
			die("SDL_WaitEvent failed: %s", SDL_GetError());
		switch (event.type) {
		case SDL_QUIT: {
			running = false;
		} break;
		case SDL_WINDOWEVENT: {
			if (event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
				window_width = event.window.data1;
				window_height = event.window.data2;
			}
		} break;
		case SDL_TEXTINPUT: {
			uint64_t length = strlen(event.text.text);
			// TODO: UTF8
			assert(length < 2);
			buffer_insert(&buffer, cursor++, event.text.text, length);
			cursor_column++;
			cursor_width = glyphs[(int) event.text.text[0]].width;
			cursor_height = glyphs[(int) event.text.text[0]].height;
		} break;
		case SDL_KEYDOWN: {
		//case SDL_KEYUP:
			#define KEY_PRESSED(x) (event.key.keysym.sym == (x))
			//#define KEY_PRESSED_ONCE(x) (event.key.repeat == 0 && event.key.keysym.sym == (x))
			#define KEY_PRESSED_MOD(m, x) (event.key.repeat == 0 && (event.key.keysym.mod & (m)) && event.key.keysym.sym == (x))

			if (KEY_PRESSED(SDLK_RETURN)) {
				buffer_insert(&buffer, cursor++, "\n", 1);
				cursor_column = 0;
				cursor_row++;
			} else if (KEY_PRESSED(SDLK_LEFT) && cursor > 0) {
				cursor--;
				if (cursor_column == 0) {
					cursor_row--;
				}
			} else if (KEY_PRESSED(SDLK_RIGHT) && cursor < buffer.length) {
				cursor++;
			} else if (KEY_PRESSED(SDLK_BACKSPACE) && cursor > 0) {
				buffer_remove_left(&buffer, cursor, 1);
				cursor--;
			} else if (KEY_PRESSED(SDLK_DELETE) && cursor < buffer.length) {
				buffer_remove_right(&buffer, cursor, 1);
			}

			#undef KEY_PRESSED_MOD
			#undef KEY_PRESSED
		} break;
		}
		SDL_SetRenderDrawColor(renderer, background_color.r, background_color.g, background_color.b, background_color.a);
		SDL_RenderClear(renderer);
		uint64_t line = 0;
		uint64_t column = 0;
		for (uint64_t i = 0; i < buffer.gap_start; i++) {
			uint32_t x = (uint32_t) buffer.text[i];
			if (x == '\n') {
				line++;
				column = 0;
				continue;
			}
			SDL_Rect destination = {(int) (column * glyphs[x].width), (int) (line * glyphs[x].height), (int) glyphs[x].width, (int) glyphs[x].height};
			SDL_RenderCopy(renderer, glyphs[x].texture, NULL, &destination);
			column++;
		}
		for (uint64_t i = buffer.gap_end; i < buffer_allocated_size(&buffer); i++) {
			uint32_t x = (uint32_t) buffer.text[i];
			if (x == '\n') {
				line++;
				column = 0;
				continue;
			}
			SDL_Rect destination = {(int) (column * glyphs[x].width), (int) (line * glyphs[x].height), (int) glyphs[x].width, (int) glyphs[x].height};
			SDL_RenderCopy(renderer, glyphs[x].texture, NULL, &destination);
			column++;
		}
		SDL_SetRenderDrawColor(renderer, foreground_color.r, foreground_color.g, foreground_color.b, foreground_color.a);
		SDL_Rect cursor_rect = {(int) ((cursor / (line + 1)) * cursor_width), (int) (line * cursor_height), cursor_width, cursor_height};
		/*if (buffer_char_at(&buffer, cursor == 0 ? cursor : cursor - 1) == '\n') {
			cursor_rect.x = 0;
		}*/
		SDL_RenderFillRect(renderer, &cursor_rect);
		SDL_RenderPresent(renderer);
	}
	for (uint32_t i = 0; i < MAX_GLYPHS; i++) {
		SDL_DestroyTexture(glyphs[i].texture);
	}
	TTF_CloseFont(font);
	TTF_Quit();
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
	exit(0);
}
