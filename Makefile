LIBS=sdl2 SDL2_ttf

CC=clang
CFLAGS=-Wall -Wpedantic -Wextra -ggdb $(shell pkg-config --cflags ${LIBS})
LDFLAGS=$(shell pkg-config --libs ${LIBS})

edit: edit.cpp buffer.cpp
	${CC} ${CFLAGS} -o $@ $< ${LDFLAGS}

clean:
	rm edit
